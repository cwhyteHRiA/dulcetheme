<?php /* Template Name: Home Page */ ?>
<?php get_header(); ?>

			<div id="content">
			<div class="hero" style="background-image: url('<?php the_field('hero'); ?>');background-position:center;background-repeat:no-repeat;"> 
			</div>
				<div id="inner-content" class="cf">
						
						<main id="main" class="m-all" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
							<div class="leblurb">
								<div class="wrap">
								<div class="blurbImg">
									<div id="circles">
										<img src="<?php the_field('big_circle_image'); ?>" class="bigCircle"/>
										<img src="<?php the_field('small_circle_image'); ?>" class="smallCircle"/>
									</div>
								</div>
								<div class="blurb">
									<h1><?php the_field('blurb_title'); ?></h1>
									<h2><?php the_field('sub_title'); ?></h2>
									<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
										<?php the_content(); ?>
										<a href="/about">Find Out More</a>
									<?php endwhile; endif; ?>
								</div>
							</div>
								</div>
							<!-- <div class="updates">
								<div class="news wrap">
									<span class="blogTitle">
										<img src="/wp-content/themes/dulceTheme/library/images/blogIcon.png" alt="News Icon">
										<h2>BLOG</h2>
									</span>
									<?php query_posts(array('orderby'=>'title','order'=>'DESC','posts_per_page' => '4'));
										if ( have_posts() ) :
										echo '<ul>';
    									while ( have_posts() ) : the_post(); ?>
        									<li><a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'square-200' ); ?><br /><h3><?php the_title() ?></h3><p><?php echo wp_trim_words( get_the_content(), 10 ); ?></p></a></li><?php
    									endwhile;
										endif;
									wp_reset_query(); ?></ul>
									<span class="blogTitle">
										<p><a href="/blog">READ ALL BLOG ENTRIES</a></p>
									</span>
								</div>
							</div> --!>
						</main>

				</div>

			</div>

<?php get_footer(); ?>