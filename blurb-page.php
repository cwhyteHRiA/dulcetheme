<?php /* Template Name: blurb Page with Sidebar */ ?>
<?php get_header(); ?>

			<div id="content">
			<?php if(get_field('hero')){
								echo '<div class="hero" style="background-image: url(' . "'"  . get_field('hero') . "'" .');background-position:center;background-repeat:no-repeat;"></div>' ;}?>
				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
									<h2 class="page-title"><?php if( get_field('sub-title') ): ?>
										<?php the_field('sub-title'); ?>
										<?php endif; ?>
								</header> <?php // end article header ?>

								<section class="entry-content cf" itemprop="articleBody">
									<?php the_content(); ?>
								</section> <?php // end article section ?>

							</article>

							<?php endwhile; endif; ?>
							<div id="blurbInterior">
								<ul>
									<li><img src="<?php the_field('top_circle_image'); ?>" class="topCircle"/><p><?php the_field('BlurbTop');?></p></li>
									<li><img src="<?php the_field('bottom_circle_image'); ?>" class="bottomCircle"/><p><?php the_field('BlurbBottom');?></p></li>
								</ul>
							</div>
							
						</main>

						<div id="sidebarLinks" class="sidebar m-all t-1of3 d-2of7 last-col cf" role="complementary">
							<img src="/wp-content/themes/dulceTheme/library/images/linksIcon.png" alt_text="quick links icon">
							<h2>Quick Links</h2> 
							<ul>
							<?php if(get_field('url_1')){
								echo '<li><a href="' . get_field('url_1') . '" target="_blank">' . get_field('title_1') . '</a>' . get_field('description_1') . '</li>' ;}?>
							<?php if(get_field('url_2')){
								echo '<li><a href="' . get_field('url_2') . '" target="_blank">' . get_field('title_2') . '</a>' . get_field('description_2') . '</li>' ;}?>
							<?php if(get_field('url_3')){
								echo '<li><a href="' . get_field('url_3') . '" target="_blank">' . get_field('title_3') . '</a>' . get_field('description_3') . '</li>' ;}?>
							<?php if(get_field('url_4')){
								echo '<li><a href="' . get_field('url_4') . '" target="_blank">' . get_field('title_4') . '</a>' . get_field('description_4') . '</li>' ;}?>
							<?php if(get_field('url_5')){
								echo '<li><a href="' . get_field('url_5') . '" target="_blank">' . get_field('title_5') . '</a>' . get_field('description_5') . '</li>' ;}?>
							<?php if(get_field('url_6')){
								echo '<li><a href="' . get_field('url_6') . '" target="_blank">' . get_field('title_6') . '</a>' . get_field('description_6') . '</li>' ;}?>
							<?php if(get_field('url_7')){
								echo '<li><a href="' . get_field('url_7') . '" target="_blank">' . get_field('title_7') . '</a>' . get_field('description_7') . '</li>' ;}?>
							<?php if(get_field('url_8')){
								echo '<li><a href="' . get_field('url_8') . '" target="_blank">' . get_field('title_8') . '</a>' . get_field('description_8') . '</li>' ;}?>
							<?php if(get_field('url_9')){
								echo '<li><a href="' . get_field('url_9') . '" target="_blank">' . get_field('title_9') . '</a>' . get_field('description_9') . '</li>' ;}?>
							<?php if(get_field('url_10')){
								echo '<li><a href="' . get_field('url_10') . '" target="_blank">' . get_field('title_10') . '</a>' . get_field('description_10') . '</li>' ;}?>
							<?php if(get_field('url_11')){
								echo '<li><a href="' . get_field('url_11') . '" target="_blank">' . get_field('title_11') . '</a>' . get_field('description_11') . '</li>' ;}?>
							<?php if(get_field('url_12')){
								echo '<li><a href="' . get_field('url_12') . '" target="_blank">' . get_field('title_12') . '</a>' . get_field('description_12') . '</li>' ;}?>
							</ul>
						</div>
				</div>

			</div>

<?php get_footer(); ?>
