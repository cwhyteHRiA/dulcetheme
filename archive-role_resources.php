<?php get_header(); ?>

			<div id="content">
				<div class="hero" style="background-image: url('http://dulceproject.org/wp-content/uploads/2016/10/SleepingBaby.jpg');background-position:center;background-repeat:no-repeat;"></div>
				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
							<br /><br />
							<h1 class="page-title">Continous Quality Improvement (CQI)</h1>
							<table class="tablesorter">
<thead>
<tr>
<th width="30%">Name</th>
<th width="20%">Date</th>
<th width="20%">Role</th>
<th width="20%">Topic</th>
<th width="20%">Type</th>
</tr>
</thead>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">

								<tr>
<td>
<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
<td><?php the_field('date');?></td>
<td><?php the_field('role');?></td>
<td><?php the_field('topic');?></td>
<td><?php the_field('type');?></td>
</tr>

							</article>

							<?php endwhile; ?>
								</table>

<br /><br />
									<?php bones_page_navi(); ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the archive.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>
					</div>

				</div>

			</div>

<?php get_footer(); ?>
