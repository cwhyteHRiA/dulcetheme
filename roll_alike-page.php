<?php /* Template Name: Roll Alike */ ?>
<?php get_header(); ?>

			<div id="content">
			<?php if(get_field('hero')){
								echo '<div class="hero" style="background-image: url(' . "'"  . get_field('hero') . "'" .');background-position:center;background-repeat:no-repeat;"></div>' ;}?>
				<div id="inner-content" class="wrap cf">

					<main id="main" class="m-all" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
						<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
						<div class="facetResources">
							<ul>
							
							<li><strong>Type</strong><?php echo facetwp_display( 'facet', 'type' ); ?></li>
							</ul>
							<ul>
							
							<li><strong>Uploaded By</strong><br/>Site Name<?php echo facetwp_display( 'facet', 'site_name' ); ?></li>
							<li><br/>Role<?php echo facetwp_display( 'facet', 'role' ); ?></li>
							</ul>
							<button onclick="FWP.reset()">Reset</button>
						</div>	
							<?php echo facetwp_display( 'template', 'resources' ); ?>
						</main>

				</div>

			</div>

<?php get_footer(); ?>
