			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
				<div class="top-footer">
					<div id="top" class="wrap">
						<nav role="navigation">
							<?php wp_nav_menu(array(
    						'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
    						'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
    						'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
    						'menu_class' => 'nav footer-nav cf',            // adding custom nav class
    						'theme_location' => 'footer-links',             // where it's located in the theme
							)); ?>
							
						</nav>
						
					</div>
					<div id="bottom" class="wrap">
						<ul>
							<li><a href=""><img src="/wp-content/uploads/2016/06/round-logo.png" alt="CSSP Logo"></a></li>
						</ul>
					</div>
				</div>

				<div id="inner-footer" class="wrap cf">

					

					<p class="source-org copyright">&copy; <?php echo date('Y'); ?> Project DULCE</p>

				</div>

			</footer>

		</div>

		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
