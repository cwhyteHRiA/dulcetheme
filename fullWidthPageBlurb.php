<?php /* Template Name: Full Width Page with Blurb */ ?>
<?php get_header(); ?>

			<div id="content">
			<div class="hero" style="background-image: url('<?php the_field('hero'); ?>');background-position:center;background-repeat:no-repeat;"> 
			</div>
				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
									<h2 class="page-title"><?php if( get_field('sub-title') ): ?>
										<?php the_field('sub-title'); ?>
										<?php endif; ?>
									</h2>

								</header> <?php // end article header ?>

								<section class="entry-content cf" itemprop="articleBody">
									<?php the_content(); ?>
								</section> <?php // end article section ?>

								<footer class="article-footer cf">

								</footer>

							</article>

							<?php endwhile; endif; ?>
						</main>

				</div>

			</div>
			<div id="blurbInterior">
				<div class="wrap">
								<ul>
									<li><img src="<?php the_field('top_circle_image'); ?>" class="topCircle"/><p><?php the_field('BlurbTop');?></p></li>
									<li><img src="<?php the_field('bottom_circle_image'); ?>" class="bottomCircle"/><p><?php the_field('BlurbBottom');?></p></li>
								</ul>
							</div>
				</div>
<?php get_footer(); ?>

