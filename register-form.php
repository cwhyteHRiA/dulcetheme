<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<div class="tml tml-register" id="theme-my-login<?php $template->the_instance(); ?>">
	<?php $template->the_action_template_message( 'register' ); ?>
	<?php $template->the_errors(); ?>
	<form name="registerform" id="registerform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'register', 'login_post' ); ?>" method="post">
		<?php if ( 'email' != $theme_my_login->get_option( 'login_type' ) ) : ?>
		<p class="tml-user-login-wrap">
			<label for="user_login<?php $template->the_instance(); ?>"><?php _e( 'Username', 'theme-my-login' ); ?></label>
			<input type="text" name="user_login" id="user_login<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_login' ); ?>" size="20" />
		</p>
		<?php endif; ?>

		<p class="tml-user-email-wrap">
			<label for="user_email<?php $template->the_instance(); ?>"><?php _e( 'E-mail', 'theme-my-login' ); ?></label>
			<input type="text" name="user_email" id="user_email<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_email' ); ?>" size="20" />
		</p>
<p class="tml-user-fname-wrap">
			<label for="user_fname<?php $template->the_instance(); ?>"><?php _e( 'First Name', 'theme-my-login' ); ?></label>
			<input type="text" name="user_fname" id="user_fname<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_fname' ); ?>" size="20" />
		</p>
        
        <p class="tml-user-lname-wrap">
			<label for="user_lname<?php $template->the_instance(); ?>"><?php _e( 'Last Name', 'theme-my-login' ); ?></label>
			<input type="text" name="user_lname" id="user_lname<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_lname' ); ?>" size="20" />
		</p>
 <p class="tml-user-sitename-wrap">
            <label for="user_sitename<?php $template->the_instance(); ?>"><?php _e( 'Site Name', 'theme-my-login' ); ?></label>
            <input type="text" name="user_sitename" id="user_sitename<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_sitename' ); ?>" size="20" />
        </p>
<p class="tml-user_team_designation-wrap">
            <label for="user_team_designation<?php $template->the_instance(); ?>"><?php _e( 'Team Designation', 'theme-my-login' ); ?></label>
            <input type="text" name="user_team_designation" id="user_team_designation<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_team_designation' ); ?>" size="20" />
        </p>

		<?php do_action( 'register_form' ); ?>

		<p class="tml-registration-confirmation" id="reg_passmail<?php $template->the_instance(); ?>"><?php echo apply_filters( 'tml_register_passmail_template_message', __( 'Registration confirmation will be e-mailed to you.', 'theme-my-login' ) ); ?></p>

		<p class="tml-submit-wrap">
			<input type="submit" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="<?php esc_attr_e( 'Register', 'theme-my-login' ); ?>" />
			<input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'register' ); ?>" />
			<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
			<input type="hidden" name="action" value="register" />
		</p>
	</form>
	<?php $template->the_action_links( array( 'register' => false ) ); ?>
</div>
