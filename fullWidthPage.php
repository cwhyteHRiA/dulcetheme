<?php /* Template Name: Full Width Page */ ?>
<?php get_header(); ?>

			<div id="content">
			<?php if(get_field('hero')){
								echo '<div class="hero" style="background-image: url(' . "'"  . get_field('hero') . "'" .');background-position:center;background-repeat:no-repeat;"></div>' ;}?>
				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
									<!-- <h2 class="page-title"><?php if( get_field('sub-title') ): ?>
										<?php the_field('sub-title'); ?>
										<?php endif; ?>
									</h2> --!>

								</header> <?php // end article header ?>

								<section class="entry-content" itemprop="articleBody">
									
									<?php the_content(); ?>
									
								</section> <?php // end article section ?>
								<?php if(get_field('small_circle_image')){
								echo '<div id="circles"><img src="' . get_field('small_circle_image') . '"class="smallCircle"/><img src="' . get_field('big_circle_image') . '"class="bigCircle"/>  </div>' ;}?>
								
							</article>
	
							<?php endwhile; endif; ?>
							
						</main>

				</div>

			</div>

<?php get_footer(); ?>

